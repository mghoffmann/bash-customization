### BASH CUSTOMIZATION ###
To install, source all the .sh files in this directory:

    $ cd <YOUR_REPO_DIRECTORY>
    $ source *.sh


Eventually, I want to make a makefile that appends the above script onto ~/.bash_aliases if it's not already in it, but I don't know how to do that yet.
