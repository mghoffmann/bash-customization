#!/bin/bash

# Usage:
# $ sethelper command_that_displays_help_text_including_arguments
# $ helpwith some_text_to_search_the_helper_command_output_for
# $ bighelp # Shows the full output of th helper command in nano for easy browsing.

# Example:
#
# $ sethelper iptables --help
#
# Now when helpwith is called, the output of
# iptables --help
# will be searched.
# 
# $ helpwith -A
#
# This will show what iptables --help says about -A:
#
#   --append  -A chain		Append to chain
#
#
# Super useful, but maybe I want to search more navigably.
# $ bighelp
#
# This will show the full output of the helper command,
# in a .tmp file in nano. The .tmp file is deleted after
# nano is exited.
#
#
# If you want to just execute the command passed to sethelper,
# it's aliased for you as helper:
#
# $ helper


# Removes the .tmp files created by other functions in this file
function clearhelper() {
  rm ~/.sethelper.tmp;
  rm ~/.helpwith.tmp;
}


# Pass the bash command, with arguments, to
# this function to set the bash script to be executed
# by the helpwith function.
function sethelper() {
  echo "# Created by sethelper defined in helpsearch.sh" > ~/.sethelper.tmp;
  echo "##############################################" >> ~/.sethelper.tmp;
  echo "                                              " >> ~/.sethelper.tmp;
  echo $@ >> ~/.sethelper.tmp;
  alias helper='bash ~/.sethelper.tmp';
}


# Pass a term to search the output of the command
# passed to sethelper for.
# This uses grep and is not case-sensitive.
function helpwith() {
  echo $@ > ~/.helpwith.tmp;
  bash ~/.sethelper.tmp | grep -i -f ~/.helpwith.tmp;
}



# Show the full output of the command passed to sethelper, in
# the sensible editor (see select-editor(1)) or nano for easy scrolling and Ctrl+W searching.
function bighelp() {
  bash ~/.sethelper.tmp > ~/.bighelp.tmp;
  ${EDITOR:-nano} ~/.bighelp.tmp;
  rm ~/.bighelp.tmp;
}
