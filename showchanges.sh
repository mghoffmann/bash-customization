#!/bin/bash

# Usage:
# In terminal 1:
# $ showchanges command_that_does_something
#
# In a separate application or terminal instance, do something
# that you expect to change the output of command_that_does_something.
#
# Back in terminal 1, press any key to have command_that_does_something
# run again, with the output from the first run diff'd with the output of the
# second run.


# Example:
#
# $ showchanges cat my_log_file.log
#
# (in another terminal)
# $ my_logging_application
#
# (in the original terminal, press any key).

function showchanges() {

  # Make a script to run that contains everything the caller passed in
  echo $@ > ~/.showchanges.run.tmp;

  # Run that script, and write it to a before file
  bash ~/.showchanges.run.tmp > ~/.showchanges.before.tmp;
  # Show the user the result too
  cat ~/.showchanges.before.tmp;

  echo;
  echo;

  # Wait for a key press
  read -n 1 -r -p "Do the thing, and then press any key to continue and compare the difference in the supplied command's output.";

  echo;
  echo;

  # Run the script again, and write it to an after file
  bash ~/.showchanges.run.tmp > ~/.showchanges.after.tmp;

  # Write the diff output to changes.changes
  diff --suppress-common-lines ~/.showchanges.before.tmp ~/.showchanges.after.tmp > changes.changes;
  # Show the user the changes
  cat changes.changes;

  # Clean up all the files except changes.changes.
  rm ~/.showchanges.run.tmp ~/.showchanges.before.tmp ~/.showchanges.after.tmp;

  echo "Diff output saved to changes.changes";
}
